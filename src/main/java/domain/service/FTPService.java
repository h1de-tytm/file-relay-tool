package domain.service;

import domain.model.Server;

public interface FTPService {
	void get(Server server);
}
