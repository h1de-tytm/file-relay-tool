package domain.repository;

import java.io.File;
import java.util.List;

public interface ServerRepository {
	File save(File file);
	List<String> showFiles();
}
